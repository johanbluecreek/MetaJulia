
head = [:sin, :cos, :log, :exp, :+, :-, :*, :/]
#head = [:sin, :cos, :exp, :+, :-, :*, :/]
#head = [:sin, :cos, :log, :exp]
#head = [:+, :-, :*, :/]
tail = [i for i=0:9]

head = vcat(head, tail)

head_l = 10
tail_l = head_l+1

function callable(expr)
    try
        eval(Expr(:call, expr, 1))
        return true
    catch
        return false
    end
end

function eargs(expr)
    if expr in [:sin, :cos, :log, :exp]
        return 1
    elseif expr in [:-, :/]
        return 2
    elseif expr in [:+, :*]
        return Inf
    else
        return NaN
    end
end

function genelist()
    elist = Any[]
    for i in 1:head_l
        push!(elist, rand(head))
    end

    for i in 1:tail_l
        push!(elist, rand(tail))
    end

    return elist
end

"""
    buildexpr(elist)

A recursive function forming mathematical expressions from an elist.

# Returns

It will return a tuple of `(Expr, nelist)`, where `Expr` is the expression that it built,
and `nelist` is the list of the unused trailing entries in the original `elist`.
"""
function buildexpr(elist)
    if eargs(elist[1]) == 1
        expr = buildexpr(elist[2:end])
        # Only one argument is taken here, the list of unused should be returned
        return Expr(:call, elist[1], expr[1]), expr[2]
    elseif eargs(elist[1]) == 2 || eargs(elist[1]) == Inf
        if isequal(eargs(elist[2]), NaN)
            # If elist[2] is a terminator (eargs() == NaN), then the expression must either
            # truncate "completely" (if also elist[3] is a terminator) or grow to the right
            # (or in the second argument, that is).
            if isequal(eargs(elist[3]), NaN)
                # Truncate completely
                return Expr(:call, elist[1], elist[2], elist[3]), elist[4:end]
            else
                expr = buildexpr(elist[3:end])
                return Expr(:call, elist[1], elist[2], expr[1]), expr[2]
            end
        else
            # If it is not a terminator at elist[2], then the expression grows to the
            # left (first argument), and the second argument is...
            expr = buildexpr(elist[2:end])
            if isequal(eargs(expr[2][1]), NaN)
                return Expr(:call, elist[1], expr[1], expr[2][1]), expr[2][2:end]
            else
                expr2 = buildexpr(expr[2])
                return Expr(:call, elist[1], expr[1], expr2[1]), expr2[2]
            end
        end

    else
        return elist[1], elist[2:end]
    end
end

function lazy_eval(expr)
    try
        return eval(expr[1])
    catch
        return Inf
    end
end

#=
for i in 1:20
    elist = genelist()
    expr = buildexpr(elist)
    pexpr = lazy_eval(expr)
    println("$elist: $expr: $pexpr \n")
end
=#

#=
elist = Any[:*, :exp, 4, :*, 2, 3, 0, 1, 4]
println("$(buildexpr(elist))")
println("$(lazy_eval(buildexpr(elist)))")
=#


elist = genelist()
println(elist)
expr = buildexpr(elist)
pexpr = lazy_eval(expr)
while !(isequal(pexpr, NaN))
    elist = genelist()
    println(elist)
    expr = buildexpr(elist)
    pexpr = lazy_eval(expr)
end
println("$elist: $expr: $pexpr \n")


#elist = Any[:-, :/, :-, :+, 7, 5, :cos, 6, :cos, 4, 1, 1, 6, 9, 9, 0, 3, 3, 1, 0, 7]
#println("$(buildexpr(elist))")

#=
elist = Any[:*, :cos, :/, :sin, :cos, 7, 1, 4, 8, 5, 6, 4, 3, 6, 0]
println("$(buildexpr(elist))")
=#

#=
elist = Any[:/, :log, :*, :log, :+, 7, 1, 5, 1, 7, 2, 9, 0, 5, 7]
println("$(buildexpr(elist)[1])")
=#

#=
elist = [:+, :+, 1,2,3,4]
println("$(buildexpr(elist))")


elist = [:+, 1, :+, 2, :+, 3, :+, 4, 5]
println("$(buildexpr(elist))")
=#

#=
elist = [:*, :sin, :/, :cos, 1, 2, 3, 4, 5, 6, 7, 8]
println("$(dump(buildexpr(elist)))")
elist = [:log, :sin, :/, :cos, 1, 2, 3, 4, 5, 6, 7, 8]
println("$(dump(buildexpr(elist)))")
=#
